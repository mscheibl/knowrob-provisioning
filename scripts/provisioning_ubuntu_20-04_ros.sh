#!/bin/bash

sudo apt install -y curl
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
sudo apt update
sudo apt install -y ros-noetic-desktop-full
sudo apt install -y ros-noetic-xacro
sudo apt install -y ros-noetic-joint-state-publisher
sudo apt install -y ros-noetic-joint-state-publisher-gui
sudo apt install -y ros-noetic-robot-state-publisher
sudo apt install -y python3-roslaunch
pip install roslibpy
echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc
source ~/.bashrc
sudo apt install -y python3-rosdep
sudo apt install -y python3-rosdep2
sudo apt install -y python3-rosinstall
sudo apt install -y python3-rosinstall-generator
sudo apt install -y python3-wstool
build-essential
sudo rosdep init
rosdep update
