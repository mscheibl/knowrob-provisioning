#!/bin/bash

sudo apt install -y ros-noetic-rosbridge-server
cd ~/catkin_ws/
catkin_make
sudo rosdep init
rosdep update
