#!/bin/bash

sudo apt install -y python3-rosdep2
rosdep update
source /opt/ros/noetic/setup.bash
mkdir -p ~/catkin_ws/src
cd ~/catkin_ws/
catkin_make
source ~/catkin_ws/devel/setup.bash
echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
cd ~/catkin_ws/src
wstool init
wstool merge https://raw.github.com/knowrob/knowrob/master/rosinstall/knowrob-base.rosinstall
wstool update
rosdep install -y --ignore-src --from-paths .
cd ~/catkin_ws
catkin_make
export SWI_HOME_DIR=/usr/lib/swi-prolog
echo "export SWI_HOME_DIR=/usr/lib/swi-prolog" >> ~/.bashrc
