#!/bin/bash

sudo apt install -y ros-noetic-rosbridge-server
pip install jsonify
pip install markupsafe==2.0.1
pip install roslibpy
pip install tqdm
pip install scipy
pip install flask_restful
pip install pandas
