#!/bin/bash

sudo apt install -y idle3
sudo apt install -y pip
sudo apt install -y python3-tk
pip install -U numpy
pip install pybullet
pip install pymongo
pip install pyzmq
pip install zmq
cd ~
git clone https://gitlab.ub.uni-bielefeld.de/mscheibl/robcogbridge.git
cd ~/robcogbridge
git submodule update --init --recursive --remote
cd ~
