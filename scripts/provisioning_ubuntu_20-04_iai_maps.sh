#!/bin/bash

cd ~/catkin_ws/src
sudo apt install -y git
git clone https://github.com/code-iai/iai_maps.git
cd ~/catkin_ws/
catkin_make
