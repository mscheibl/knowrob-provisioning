# KnowRob Provisioning

## Purpose
Bash Script to provision an Ubuntu 20.04 system fully to get started with KnowRob.

## Installed Tools
Installing ROS, Prolog, Robo3T, MongoDB, all necessary Python Packages and the NEEMViewer.

## Requirements
* Sudo Rights on the system.
* The system needs an internet connection.

## How to use
1. Have a system with **Ubuntu 20.04** (**Minimal Install** is sufficient, **Desktop Version**) installed.
2. **Install git** on the system (`sudo apt install git`).
3. **Clone the repository** onto your system.
4. **Go into the folder** you just cloned/copied (`cd <path to repository>`).
5. **Execute the file** `provisioning_ubuntu_20-04_main.sh` with the command `./provisioning_ubuntu_20-04_main.sh`.
6. You will be **prompted for your user password** once since some commands need sudo rights to be executed.
7. **Wait**. The install will take several minutes.

## Additional steps when used on VM
* The tool was developed with the idea of having it run on a VM (VirtualBox as Hypervisor) and to interface with the NEEMViewer from the host system
* When running it on a VM, the **VM needs a Host-only Adapter** to communicate with the host system.

1. Click on Configure while having the VM selected.<br>
![Click on Configure while having the VM selected.](images/1.png)
2. Click on Network in the window that popped up.<br>
![Click on Network in the window that popped up.](images/2.png)
3. Select the registry "Adapter 2".<br>
![Select the registry \"Adapter 2\".](images/3.png)
4. Activate the adapter and select it to be a Host-only Adapter. Then confirm the configuration with "OK".<br>
![Activate the adapter and select it to be a Host-only Adapter. Then confirm the configuration with \"OK\".](images/4.png)

## Troubleshooting
* If the terminal responds with a `Permission Denied` error, check if all the scripts in the repository are executable. To make all the scripts executable, change into the repository (`cd <path to repository>`). Then enter `chmod +x provisioning_*`
