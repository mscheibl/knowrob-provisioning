#!/bin/bash
GREEN='\033[0;32m'
NC='\033[0m' # No Color

printf ">>> Starting ${GREEN}ROS${NC} Installation<<<\n"
./scripts/provisioning_ubuntu_20-04_ros.sh

printf ">>> Starting ${GREEN}ROS Bridge${NC} Installation<<<\n"
./scripts/provisioning_ubuntu_20-04_rosbridge.sh

printf ">>> Starting ${GREEN}Prolog${NC} Installation<<<\n"
./scripts/provisioning_ubuntu_20-04_prolog.sh

printf ">>> Starting ${GREEN}MongoDB${NC} Installation<<<\n"
./scripts/provisioning_ubuntu_20-04_mongodb.sh

printf ">>> Starting ${GREEN}KnowRob${NC} Installation<<<\n"
./scripts/provisioning_ubuntu_20-04_knowrob.sh

printf ">>> Starting ${GREEN}Robo3T${NC} Installation<<<\n"
./scripts/provisioning_ubuntu_20-04_robo3t.sh

printf ">>> Starting ${GREEN}KnowRob Helper${NC} Installation<<<\n"
./scripts/provisioning_ubuntu_20-04_robcogbridge.sh

printf ">>> Starting ${GREEN}REST Server${NC} Installation<<<\n"
./scripts/provisioning_ubuntu_20-04_rest_server.sh

printf ">>> Starting ${GREEN}IAI Maps${NC} Installation<<<\n"
./scripts/provisioning_ubuntu_20-04_iai_maps.sh

print ">>> Rebooting System now<<<"
echo "Press any key to continue..."
read -s -n 1
reboot
